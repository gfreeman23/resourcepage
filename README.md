**Single Page Resource Guide**

[https://gregfreeman.me/projects/resource-page/](https://gregfreeman.me/projects/resource-page/)

---

- Single Page Responsive Event Landing Page
- HTML, CSS, JS, jQuery
- Back to Top Button
- Tab Navigation
- Accordions
- Modals
